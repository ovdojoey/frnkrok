(function(){

  'use strict';
  var aa = [].slice.call(document.querySelectorAll('.animate'));
  var menuLinks = [].slice.call(document.querySelectorAll('.menu-item'));
  var dimTri = document.getElementById("dim-tri");
  var triExit = document.getElementById("dim-exit");
  var exits = [].slice.call(document.querySelectorAll(".exit-to-home"));
  var subForm = document.getElementById("subscribe-form");
  var moreOnFrankBtn = document.getElementById("more-on-frank-btn");
  var socialIconBtns = document.getElementById("social-home-btns");
  var showMoreOnFrankBtn = true;


  var localStorageSupport = true,
    emailAdded = false,
    emailInput = document.getElementById("email_input"),
    redirectLoc;

  var blocks = {
    events : document.getElementById("evts-block"),
    booking : document.getElementById("booking-block"),
    social : document.getElementById("social-block"),
    tracks : document.getElementById("tracks-block"),
    edits : document.getElementById("edits-block"),
    signup : document.getElementById("signup-block"),
    about : document.getElementById("about-frank"),
    mastering : document.getElementById("mastering-block"),
  };


  function fireAnimations () {
    aa.forEach( function (ele) {
      ele.classList.add('aa');
    } );
  }

  function swapVideoWithGif () {

    var _video_backup = document.getElementById("frnk-video-backup"),
      _video = document.getElementById("frnk-video");

    _video.style.display = "none";
    _video_backup.style.display = "block";

    try {
      _video.src = "";
      _video.load();
      _video.remove();
    } catch (e) {
      console.log(e);
    }
  }

  function testForVideoAutoplay () {
    var _video = document.getElementById("frnk-video"),
      _timer,
      _delayPlay = false;

    if (!_video.paused) {
      return false;
    }

    _timer = setTimeout(function () {

      if (!_video.paused || _delayPlay) {
        return false;
      }

      swapVideoWithGif();
      return false;

    }, 4200);

    _video.addEventListener('playing', function () {

      _delayPlay = true;
      clearTimeout(_timer);

    }, false);

  }

  function testLocalSupport () {

    if (!window.localStorage) {
      console.log("LocalStorage not supported by the browser");
      localStorageSupport = false;
      return false;
    }

    try {
        localStorage.setItem('testItem', 1);
    } catch (e) {
        console.log("LocalStorage not supported by the browser - Possible viewing in Private Mode");
        localStorageSupport = false;
        return false;
    }

    var _emailIn = localStorage.getItem('emailAdded');
    if(_emailIn) {
      emailAdded = true;
    }

    return true;
  }

  function validateEmail(email) {
      var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      return re.test(email);
  }

  function subscribeEmail (event) {

    event.preventDefault();

    var _messageBox = document.getElementById("message_sub");
    if ( !emailInput.value || !validateEmail(emailInput.value) ) {
      _messageBox.innerHTML = "Please enter a valid email address";
      _messageBox.classList.add("show");
      return false;
    }

    var xmlhttp;
    if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
    } else {
      // code for older browsers
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var response = JSON.parse(xmlhttp.responseText);
        console.log(response.status);
      }
    };
    xmlhttp.open("GET", "curlMailSub.php?email=" + emailInput.value, true);
    xmlhttp.send();

    _messageBox.classList.remove("show");

    emailAdded = true;

    if (localStorageSupport) {
      localStorage.setItem('emailAdded', 1);
    }

    if (redirectLoc === 'tracks') {
      fireTracks();
    }

    if (redirectLoc === 'edits') {
      fireEdits();
    }

    return false; // prevent form submission

  }

  function fireBooking () {

    dimTri.classList.add("booking");
    dimTri.classList.remove("small-out");
    dimTri.classList.remove("small");
    dimTri.classList.remove("small-all");
    dimTri.classList.remove("email-signup");


    blocks.booking.classList.remove("out");
    blocks.booking.classList.add("aa");

    blocks.about.classList.add("out");


    blocks.signup.classList.remove("aa");
    blocks.tracks.classList.remove("aa");
    blocks.mastering.classList.remove("aa");

    blocks.edits.classList.remove("aa");

    blocks.events.classList.remove("aa");

    blocks.social.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    // triExit.classList.add("aa");
    triExit.classList.remove("aa");

    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");
    showMoreOnFrankBtn = false;
  }

  function fireEvents () {

    dimTri.classList.add("small");
    dimTri.classList.remove("small-out");
    dimTri.classList.remove("small-all");
    dimTri.classList.remove("booking");
    dimTri.classList.remove("email-signup");


    blocks.events.classList.add("aa");

    blocks.about.classList.add("out");


    blocks.social.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.signup.classList.remove("aa");
    blocks.tracks.classList.remove("aa");
    blocks.mastering.classList.remove("aa");

    blocks.edits.classList.remove("aa");


    // booking block
    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");


    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");

    triExit.classList.add("aa");
    showMoreOnFrankBtn = false;

  }

  function fireSocial () {

    dimTri.classList.remove("small");
    dimTri.classList.add("small-all");
    dimTri.classList.remove("small-out");
    dimTri.classList.remove("booking");
    dimTri.classList.remove("email-signup");


    blocks.events.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");

    blocks.about.classList.add("out");


    blocks.signup.classList.remove("aa");
    blocks.tracks.classList.remove("aa");
    blocks.mastering.classList.remove("aa");

    blocks.edits.classList.remove("aa");

    blocks.social.classList.add("aa");

    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");

    triExit.classList.add("aa");
    showMoreOnFrankBtn = false;


  }


  function fireTracks () {


    blocks.events.classList.remove("aa");

    blocks.social.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");

    blocks.about.classList.add("out");

    blocks.mastering.classList.remove("aa");
    blocks.edits.classList.remove("aa");

    triExit.classList.remove("aa");

    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");

    showMoreOnFrankBtn = false;



    if ( !emailAdded ) {
      /** show email needed */
      dimTri.classList.remove("small");
      dimTri.classList.remove("small-all");
      dimTri.classList.remove("booking");
      dimTri.classList.remove("small-out");
      dimTri.classList.add("email-signup");

      blocks.signup.classList.add("aa");

      redirectLoc = 'tracks';

      return false;
    }



    blocks.tracks.classList.add("aa");
    dimTri.classList.add("small-out");

  }

  function fireMastering () {

    blocks.events.classList.remove("aa");

    blocks.social.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");

    blocks.about.classList.add("out");


    blocks.edits.classList.remove("aa");

    triExit.classList.remove("aa");

    blocks.tracks.classList.remove("aa");

    blocks.mastering.classList.add("aa");

    dimTri.classList.add("small-out");
    dimTri.classList.remove("small-all");

    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");

    showMoreOnFrankBtn = false;


  }

  function fireEdits () {


    blocks.events.classList.remove("aa");

    blocks.social.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.tracks.classList.remove("aa");

    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");

    blocks.about.classList.add("out");


    triExit.classList.remove("aa");


    dimTri.classList.remove("small-out");
    dimTri.classList.remove("booking");
    dimTri.classList.remove("email-signup");

    blocks.mastering.classList.remove("aa");
    moreOnFrankBtn.classList.add("hide");
    socialIconBtns.classList.add("hide");

    showMoreOnFrankBtn = false;


    if ( !emailAdded ) {
      /** show email needed */
      dimTri.classList.remove("small");
      dimTri.classList.remove("small-all");
      dimTri.classList.remove("booking");
      dimTri.classList.remove("small-out");
      dimTri.classList.add("email-signup");

      blocks.signup.classList.add("aa");

      redirectLoc = 'edits';

      return false;
    }


    triExit.classList.add("aa");
    blocks.edits.classList.add("aa");
    dimTri.classList.add("small");

  }


  function fireHome (route) {

    dimTri.classList.remove("hovered");

    if (route !== 1) {
      window.location.hash = "#/";
      window.location.replace("#/");
    }

    dimTri.classList.remove("small");
    dimTri.classList.remove("small-all");
    dimTri.classList.remove("small-out");
    dimTri.classList.remove("email-signup");
    dimTri.classList.remove("booking");


    blocks.events.classList.remove("aa");

    blocks.signup.classList.remove("aa");

    blocks.social.classList.remove("aa");

    blocks.mastering.classList.remove("aa");
    blocks.signup.classList.remove("aa");
    blocks.tracks.classList.remove("aa");
    blocks.edits.classList.remove("aa");

    blocks.booking.classList.remove("aa");
    blocks.booking.classList.add("out");

    blocks.about.classList.remove("out");

    moreOnFrankBtn.classList.remove("hide");
    socialIconBtns.classList.remove("hide");

    showMoreOnFrankBtn = true;

    triExit.classList.remove("aa");

  }

  function router () {
    var _location = location.hash;
    window.scrollTo(0,0);
    switch (_location) {
      case "#/":
        fireHome(1);
        break;
      case "#/tracks":
        fireTracks();
        break;
      case "#/edits":
        fireEdits();
        break;
      case "#/social":
        fireSocial();
        break;
      case "#/booking":
        fireBooking();
        break;
      case "#/events":
        fireEvents();
        break;
      case "#/mastering":
        fireMastering();
        break;
      default:
        fireHome(1);
    }

  }

  var scrollListen = false;
  var start = null;
  var scrollTo = null;
  var maxWindowHeight = window.innerHeight;

  /*
   * The rAF function
  */
  function step(timestamp) {


    if (!start) start = timestamp;
    var progress = timestamp - start;
    var scrollPoint = window.scrollY;

    if ( scrollPoint > 200 ) {
      moreOnFrankBtn.classList.add("hide");
    } else {
      if ( showMoreOnFrankBtn ) {
        moreOnFrankBtn.classList.remove("hide");
      }
    }

    if ( scrollTo !== null ) {

      var window_at = document.body.scrollTop || document.documentElement.scrollTop;
      var newScrollTo;

      // animate to a scroll point
      if ( window_at > scrollTo ) {

        newScrollTo = window_at - 40;

        if ( newScrollTo < scrollTo ) {
          newScrollTo = scrollTo;
        }

      } else {

        newScrollTo = window_at + 40;

        if ( newScrollTo > scrollTo  ) {
          newScrollTo = scrollTo;
        }

      }

      window.scrollTo(0, newScrollTo);

      if ( scrollTo == newScrollTo ) {
        scrollTo = null;
      }

    }

    window.requestAnimationFrame(step);

  }


  window.requestAnimationFrame(step);

  moreOnFrankBtn.addEventListener("click", function() {
    var limit = Math.max( document.body.scrollHeight, document.body.offsetHeight,
document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight ) - window.innerHeight;
    var _frankStart = document.getElementById("start-about-frank");
    var _frankOffset = _frankStart.getBoundingClientRect();
    var _tempScrollPoint = _frankOffset.top;
    if ( _tempScrollPoint > limit ) {
      _tempScrollPoint = limit;
    }
    scrollTo = _tempScrollPoint;
  });


  // temp changes to view
  testLocalSupport();
  testForVideoAutoplay();



  setTimeout(fireAnimations, 200);

  window.addEventListener("hashchange", router, false);
  router();

  window.addEventListener("resize", function() {
    maxWindowHeight = window.innerHeight;
  }, false);


  subForm.addEventListener("submit", subscribeEmail, false);
  dimTri.addEventListener("touchstart", function(e){
    // e.preventDefault();
    fireHome();
  }, false);

  // var timeoutHover = null;
  // dimTri.addEventListener("mouseenter", function(e){
  //   var _this = this;
  //   timeoutHover = setTimeout(function(){
  //     _this.classList.add("hovered");
  //   }, 300);
  // }, false);
  // dimTri.addEventListener("mouseleave", function(e){
  //   this.classList.remove("hovered");
  //   clearTimeout(timeoutHover);
  // }, false);
  dimTri.addEventListener("click", fireHome, false);
  triExit.addEventListener("click", fireHome, false);
  exits.forEach(function (ele) {
    ele.addEventListener("click", fireHome, false);
  });

})();

function imgPreloader() {

  this.images = [];

  this.addImages = function(images) {

    var self = this;

    if (!images) return;

    if (Array.isArray(images)) {
      images.forEach(function(ele) {
        var _image = new Image();
        _image.src = ele;
        self.images.push(_image);
      });
    }
  };

  return this;
}

/* To use, simply pass in an array of  images */
var newImage = new imgPreloader();
newImage.addImages(["/pictures/frnkrok_crowd_cran.png"]);
