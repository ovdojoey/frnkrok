<?php

  function validateForm() {
    $errors = null;
    if ( !$_POST['name'] ) {
      $errors .= "Missing Name! ";
    }
    if ( !$_POST['email'] ) {
      $errors .= "Missing Email! ";
    }
    if ( !$_POST['tel'] ) {
      $errors .= "Missing Telephone! ";
    }
    if ( !$_POST['project_name'] ) {
      $errors .= "Missing Project Name! ";
    }
    if ( !$_POST['comments'] ) {
      $errors .= "Missing Comments! ";
    }
    if ( $_POST['project_type'] !== "tracks" && $_POST['project_type'] !== "stems"
    && $_POST['project_type'] !== "mix-set" && $_POST['project_type'] !== "album"
    && $_POST['project_type'] !== "track-mixdowns" ) {
      $errors .= "Invalid project type! ";
    }


    if ( $errors ) {
      return $errors;
    }
    return true;
  }


  if ( $_GET['payed'] ) {
    $paySuccess = true;
  }

  if ( $_GET['success_redirect'] ) {
    if ( $_GET['project_type'] === "tracks" ) {
      $payLink = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="R82ULE5HP77NS"> <table> <tr><td><input type="hidden" name="on0" value="Number Of Tracks">Number Of Tracks</td></tr><tr><td><select name="os0">            <option value="1 Track">1 Track $50.00 USD</option>            <option value="2 Tracks">2 Tracks $100.00 USD</option>            <option value="3 Tracks">3 Tracks $150.00 USD</option>            <option value="4 Tracks">4 Tracks $200.00 USD</option>            <option value="5 Tracks">5 Tracks $250.00 USD</option>            <option value="6 Tracks">6 Tracks $300.00 USD</option> </select> </td></tr> </table> <input type="hidden" name="currency_code" value="USD"> <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"> <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">';
    }
    elseif ( $_GET['project_type'] === "stems" ) {
      $payLink = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="UY2JPXACHD4C4"> <table> <tr><td><input type="hidden" name="on0" value="Number Of Stems">Number Of Stems</td></tr><tr><td><select name="os0">            <option value="1 Stem">1 Stem $5.00 USD</option>            <option value="2 Stems">2 Stems $10.00 USD</option>            <option value="3 Stems">3 Stems $15.00 USD</option>            <option value="4 Stems">4 Stems $20.00 USD</option>            <option value="5 Stems">5 Stems $25.00 USD</option>            <option value="6 Stems">6 Stems $30.00 USD</option> </select> </td></tr> </table> <input type="hidden" name="currency_code" value="USD"> <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"> <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> </form>';
    }
    elseif ( $_GET['project_type'] === "mix-set" ) {
      $payLink = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top"> <input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="PHAYK5DZYC28G"> <table> <tr><td><input type="hidden" name="on0" value="Number Of Sets">Number Of Sets</td></tr><tr><td><select name="os0">            <option value="1 Set">1 Set $50.00 USD</option>            <option value="2 Sets">2 Sets $100.00 USD</option>            <option value="3 Sets">3 Sets $150.00 USD</option> </select> </td></tr> </table> <input type="hidden" name="currency_code" value="USD"> <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"> <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> </form>';
    } else {
      $payLink = "We are reviewing your project details and will be in touch shortly. <p>Payment options are not available until a quote is established.</p>";
    }


    $success = true;

  }
  elseif ($_POST) {
    $validate = validateForm();
    if ($validate !== true) {
      $displayErrors = $validate;
    } else {

      $name = trim(str_replace("'", "`", $_POST['name']));
      $email = trim(str_replace("'", "`", $_POST['email']));
      $tel = trim(str_replace("'", "`", $_POST['tel']));
      $project_name = trim(str_replace("'", "`", $_POST['project_name']));
      $comments = trim(str_replace("'", "`", $_POST['comments']));
      $emailString = "Name : {$name} " . "\r\n"
      . "Email : {$email}" . "\r\n"
      . "Tel : {$tel}" . "\r\n"
      . "Project Name : {$project_name}" . "\r\n"
      . "Comments : {$comments}" . "\r\n"
      . "Type : {$_POST['project_type']}" . "\r\n" ;

      $send = mail("ovdojoey@gmail.com", "Website Contact", $emailString);
      $send = mail("contactfrnkrok@gmail.com", "Website Contact", $emailString);

      if ( $send === true ) {
        header("Location:?success_redirect=1&project_type=" . $_POST['project_type']);
        exit;
      }
    }

  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>FRNKROK | Mastering Engineer</title>
  <link rel="stylesheet" type="text/css" href="/css/frnkrok.css">
  <link href='https://fonts.googleapis.com/css?family=Cabin|Oswald|Open+Sans+Condensed:300|Rock+Salt' rel='stylesheet' type='text/css'>
</head>
<body>
  <noscript>
    Javascript must be enabled to view this website.  Please enable it.
  </noscript>

  <section class="above-fold">
    <header>
      <nav class="navigation animate">
        <ul class="navi-menu">
          <a href="/#/"><li class="menu-item" data-activate="home" id="fire-home">Home</li></a>
          <a href="/#/events"><li class="menu-item" data-activate="events" id="fire-events">Events</li></a>
          <a href="/#/booking"><li class="menu-item" data-activate="booking" id="fire-booking">Booking</li></a>
          <a href="/#/social"><li class="menu-item" data-activate="social" id="fire-social">Social</li></a>
          <a href="/#/tracks"><li class="menu-item" data-activate="tracks" id="fire-tracks">Tracks</li></a>
          <a href="/#/edits"><li class="menu-item" data-activate="edits" id="fire-edits">Edits</li></a>
          <a href="/#/mastering"><li class="menu-item"data-activate="mastering" id="fire-mastering">Mastering</li></a>
        </ul>
      </nav>
    </header>

    <section class="mastering-block animate" id="mastering_block">
      <a href="/mastering/start/"><img src="/pictures/new_logo.png" alt="FRNKROK" class="logo"></a>
      <?php
      if ( $paySuccess ) {
      ?>
      <h1 class="headline start" id="mastering-project-headline"><span class="color-alt">Thank you!</span></h1>
      <div class="pay-action">
        <h2>You rock!</h2>
        <em>Please send your assets (WAV files) and any additional comments to,</em>
        <p>contactfrnkrok@gmail.com</p>
        <em>We will get started working on your project right away.</em>
      </div>
      <?php
      }
      elseif ( $success ) {
      ?>
        <h1 class="headline start" id="mastering-project-headline">Thank you!</h1>
        <div class="pay-action">
          <p>We have your contact information and will be in touch shortly.</p>
          <p>Use the payment options provided below to start your project immediately.</p>
          <hr class="pay-divide">
          <?php echo $payLink; ?>
          <hr class="pay-divide">
        </div>
      <?php
      }
      else {
      ?>
      <h1 class="headline start" id="mastering-project-headline">START A <span class="color-alt">MASTERING</span> PROJECT</h1>
      <?php
      if ($displayErrors) echo '<div class="errors" id="errors_box"><h3>unable to send</h3>' . $displayErrors . '<h4>please, try again</h4></div>';
      else echo '<div class="errors hide" id="errors_box"></div>';
      ?>
      <div class="what-mastered-block" id="what-mastered">
        <h3 class="headline about">What do you need mastered?</h3>
        <ul class="services-mastering">
          <li>
            <a href="#/mix-set" class="service">
              <div class="price">$50/set</div>
              <h3><small>a</small>DJ Mix / Set</h3>
            </a>
          </li>
          <li>
            <a href="#/tracks" class="service">
              <div class="price">$50/track</div>
              <h3><small>finished</small>Tracks</h3>
            </a>
          </li>
          <li>
            <a href="#/stems" class="service">
              <div class="price">$5/stem</div>
              <h3><small>individual</small>Stems</h3>
            </a>
          </li>
          <li>
            <a href="#/album" class="service">
              <div class="price">custom quote</div>
              <h3><small>an</small>Album</h3>
            </a>
          </li>
          <li>
            <a href="#/track-mixdowns" class="service">
              <div class="price">custom quote</div>
              <h3><small>full</small>Track Mixdowns</h3>
            </a>
          </li>
        </ul>
        <p class="tagline-satisfaction">
          Turnarounds are 3-5 business days and we offer the opportunity to go back and retouch your project if you are not satisfied.
        </p>
      </div>
      <div class="start-signup" id="start-signup">
        <div class="signup-form">
          <a class="exit" href="#/">&times;</a>
          <p>Get started by filling out the contact form below.</p>
          <form method="POST" action="/mastering/start/#/go">
            <h4 class="group-headline">Contact Details</h4>
            <label>
              Name:
              <input type="text" required="required" name="name" placeholder="Name" value="<?php echo $_POST['name']; ?>">
            </label>
            <label>
              Email:
              <input type="email" name="email" required="required" placeholder="Email" value="<?php echo $_POST['email']; ?>">
            </label>
            <label>
              Telephone:
              <input type="tel" required="required"  name="tel" placeholder="Tel" value="<?php echo $_POST['tel']; ?>">
            </label>
            <h4 class="group-headline">Project Details</h4>
            <label>
              Name of Project:
              <input type="text" required="required" name="project_name" placeholder="Project Name" value="<?php echo $_POST['project_name']; ?>">
              <input type="hidden" name="project_type" id="project_type">
            </label>
            <label>
              Comments / Notes:
              <textarea class="textarea" required="required" name="comments" placeholder="Enter the style of your project and any important notes."><?php echo $_POST['comments']; ?></textarea>
            </label>
            <button type="submit" class="continue">Continue &rarr;</button>
            <p>Click continue to send your project information and proceed to the payment page.</p>
          </form>
        </div>
      </div>
      <?php } ?>

    </section>
  <script>
  (function(){

    'use strict';
    var aa = [].slice.call(document.querySelectorAll('.animate'));
    var mastering_block =  document.getElementById("mastering_block");
    var mastering_headline =  document.getElementById("mastering-project-headline");
    var errors_box =  document.getElementById("errors_box");
    var what_mastered =  document.getElementById("what-mastered");
    var project_type =  document.getElementById("project_type");
    var mastering_headline_default = mastering_headline.innerHTML;


    function fireAnimations () {
      aa.forEach( function (ele) {
        ele.classList.add('aa');
      } );
    }

    function showForm(type) {
      mastering_block.classList.add("activate-form");
      what_mastered.classList.add("hide");
      errors_box.classList.add("hide");
      window.scrollTo(0,300);

      if (type === "tracks") {
        mastering_headline.innerHTML = '<span class="color-alt">TRACK</span> MASTERING';
      }
      if (type === "mix-set") {
        mastering_headline.innerHTML = '<span class="color-alt">MIX / SET</span> MASTERING';
      }
      if (type === "stems") {
        mastering_headline.innerHTML = '<span class="color-alt">STEM</span> MASTERING';
      }
      if (type === "album") {
        mastering_headline.innerHTML = '<span class="color-alt">ALBUM</span> MASTERING';
      }
      if (type === "track-mixdowns") {
        mastering_headline.innerHTML = '<span class="color-alt">TRACK MIXDOWNS</span> MASTERING';
      }
      project_type.value = type;

    }
    function hideForm() {
      mastering_block.classList.remove("activate-form");
      mastering_headline.innerHTML = mastering_headline_default;
      what_mastered.classList.remove("hide");
    }

    function router() {
      var _location = location.hash;
      window.scrollTo(0,0);
      switch (_location) {
        case "#/":
          hideForm();
          break;
        case "#/tracks":
          showForm("tracks");
          break;
        case "#/mix-set":
          showForm("mix-set");
          break;
        case "#/stems":
          showForm("stems");
          break;
        case "#/album":
          showForm("album");
          break;
        case "#/track-mixdowns":
          showForm("track-mixdowns");
          break;
        default:
          hideForm();
      }
    }

    setTimeout(fireAnimations, 200);

    window.addEventListener("hashchange", router, false);
    router();

  })();

  </script>
</body>
</html>
