<?php
  $apiKey = file('./mailchimpkey');
  $email = trim(strtolower( $_GET['email'] ));
  $apiKey = trim($apiKey[0]);

  if ( !$apiKey || !$apiKey || !$email) {
    header("Error, missing key or bad request", true, 400);
    return false;
  }

  $data = array("email_address" => $email, "status" => "subscribed");
  $data_string = json_encode($data);

  // Get cURL resource
  $curl = curl_init();
  // // Set some options - we are passing in a useragent too here
  // curl_setopt_array($curl, array(
  //     CURLOPT_RETURNTRANSFER => 1,
  //     CURLOPT_URL => 'https://us7.api.mailchimp.com/3.0/lists/9e67587f52/members/',
  //     CURLOPT_USERAGENT => 'FRNKROK - website',
  //     CURLOPT_POST => 1,
  //     CURLOPT_USERPWD => 'frnkrok:' . $apiKey[0],
  //     CURLOPT_POSTFIELDS => array(
  //         "email_address" => $email,
  //         "status" => 'subscribed'
  //     )
  // ));
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($curl, CURLOPT_URL, "https://us7.api.mailchimp.com/3.0/lists/be37f9f5bd/members/");
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($curl, CURLOPT_USERAGENT, 'FRNKROK - website');
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_USERPWD, "frnkrok:".$apiKey);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Content-Length: ' . strlen($data_string))
  );
  // Send the request & save response to $resp
  $resp = curl_exec($curl);
  // Close request to clear up some resources
  curl_close($curl);
  echo $resp;

 ?>
