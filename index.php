<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>FRNKROK | Orlando DJ &amp; Mix/Mastering Engineer</title>
  <link rel="stylesheet" type="text/css" href="css/frnkrok.css">
  <link href='https://fonts.googleapis.com/css?family=Cabin|Oswald|Open+Sans+Condensed:300|Rock+Salt' rel='stylesheet' type='text/css'>
</head>
<body>
  <noscript>
    Javascript must be enabled to view this website.  Please enable it.
  </noscript>

  <section class="above-fold">
    <header>
      <nav class="navigation animate">
        <ul class="navi-menu">
          <a href="#/"><li class="menu-item" data-activate="home" id="fire-home">Home</li></a>
          <a href="#/events"><li class="menu-item" data-activate="events" id="fire-events">Events</li></a>
          <a href="#/booking"><li class="menu-item" data-activate="booking" id="fire-booking">Booking</li></a>
          <a href="#/social"><li class="menu-item" data-activate="social" id="fire-social">Social</li></a>
          <a href="#/tracks"><li class="menu-item" data-activate="tracks" id="fire-tracks">Tracks</li></a>
          <a href="#/edits"><li class="menu-item" data-activate="edits" id="fire-edits">Edits</li></a>
          <a href="/mastering/start"><li class="menu-item"data-activate="mastering" id="fire-mastering">Mastering</li></a>
        </ul>
      </nav>
    </header>
    <div class="dim-container animate">
      <div class="exit" id="dim-exit">exit <div class="times">&times;</div></div>
      <div class="dim" id="dim-tri">
        <div class="logo animate">
          <img src="pictures/logo.png">
        </div>
        <video autoplay loop id="frnk-video">
          <source src="frnkrok-hq.mp4" type="video/mp4" poster="frnkrok_gif.gif" >
          Your browser does not support HTML5 video.
        </video>
        <div class="gif-backup" id="frnk-video-backup">
          <img src="frnkrok_gif.gif" role="presentation" title="Frnkrok Promo Video">
        </div>
      </div>
    </div>


    <section id="about-frank" class="about-frank-block">
      <div class="play-btn" style="display:none;">
        <div class="play-container">
          <div class="arrow-right"></div>
        </div>
      </div>
      <div class="more-on-frank-btn" id="more-on-frank-btn">
        <div class="arrow-down"><span class="arrow">&darr;</span> &nbsp; <small>about</small> FRNKROK</div>
      </div>
      <div class="social-icons" id="social-home-btns">
        <a class="symbol fb" target="_blank" href="https://www.facebook.com/FRNKROK">circlefacebook</a>
        <a class="symbol ig" target="_blank" href="https://www.instagram.com/frnkrok/">circleinstagram</a>
        <a class="symbol tw" target="_blank" href="https://www.twitter.com/frnkrok/">circletwitter</a>

      </div>
      <div class="center-pad" id="start-about-frank">
        <h1 class="headline">FRNKROK</h1>

        <img src="/pictures/Frank01.png" class="frank-img" alt="Frnkrok" title="Frank Gutierrez" />
        <p>
          At only 27, Frank Gutierrez aka FRNKROK has executive produced two nationally distributed albums in Japan and garnered International support from dance music heavy weights such as DJ CHUCKIE and Kissy Sellout. He has released a variety of Beatport singles with labels like CARILLO and 3star Deluxe, and has received support from top mix show talent such as DJ Kontrol - all while celebrating multiple year residencies.
        </p>
        <p>
          From sharing the stage with internationally renowned DJ’s to rocking parties for Fortune 500 companies, his ability to understand his audience at first glance is unmatched.
           He now finds himself contributing custom edits to DJs the world over as one of the top audio engineers for the DJ industry’s #1 source for music/music promotion, Promo Only, Inc.
           With a catalog of new remixes and original content on the way, <?php echo date("Y"); ?> looks to be a promising year.
        </p>
        <p class="links">
          <a href="#/booking">Book an Event &rarr;</a>
          <a href="/mastering/start">Mastering &rarr;</a>
        </p>

      </div>
    </section>
    <footer class="footer">
      &copy; <?php echo date("Y"); ?> Frnkrok. All rights reserved.
    </footer>

  </section>
  <section class="events" id="evts-block">
    <div class="events-container">
      <div class="events-center">
        <div class="events-left">
          <ul class="events-list">

            <?php
              $events = file("content/events.txt");
              $activatedLeft = $activatedRight = false;

              foreach ($events as $line_num => $line) {

                if (substr($line,0,7) == trim("#tracks")) {
                  break;
                }

                if (substr($line,0,13) == trim("#events-right")) {
                  $activatedRight = true;
                  $activatedLeft = false;
                  ?>
                </ul>
              </div>
              <div class="events-right">
                <ul class="events-list">
                  <?php
                  continue;
                }

                if (substr($line,0,12) == trim("#events-left")) {
                  $activatedLeft = true;
                  continue;
                }
                if ( $activatedLeft ) {
                  $event = explode(";", $line);
                  if ( !$event[0] || !$event[1] ) {
                    continue;
                  }
                  ?>
                  <li>
                    <strong class="date">
                      <?php echo $event[0]; ?>
                    </strong>
                    <span class="venue">
                      <?php echo $event[1]; ?>
                    </span>
                    <span class="loc">
                      <?php echo $event[2]; ?>
                    </span>
                  </li>
                  <?php
                }
                if ( $activatedRight ) {
                  $event = explode(";", $line);
                  if ( !$event[0] || !$event[1]) {
                    continue;
                  }
                  ?>
                  <li>
                    <strong class="date">
                      <?php echo $event[0]; ?>
                    </strong>
                    <span class="venue">
                      <?php echo $event[1]; ?>
                    </span>
                    <span class="loc">
                      <?php echo $event[2]; ?>
                    </span>
                  </li>
                  <?php
                }


              }
            ?>
            <!-- left
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE GROOVE</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE GROOVE</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li> -->
          <!-- </ul>
        </div>
        <div class="events-right">
          <ul class="events-list">
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li>
            <li><strong class="date">JAN 5 2015</strong> <span class="venue">THE SOCIAL</span> <span class="loc">ORLANDO</span></li> -->

          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="events" id="booking-block">
    <div class="events-container">
      <div class="events-center">
        <div class="row-fake">
          <h2 class="oswald headline">
            <div class="exit exit-to-home">&times;</div>
          </h2>
        </div>
        <div class="events-left">
          <div class="white-tri"></div>
            <div class="content">
              <strong>FOR ALL BOOKINGS CONTACT:</strong>
              <div class="mail-link"><a href="mailto:booking@frnkrok.com?Subject=Hey">booking@frnkrok.com</a></div>
              <div class="phone-link"><a href="tel:1-407-930-9648">(407) 930-9648</a></div>

            </div>
        </div>
        <div class="events-right">
          <div class="white-tri dark"></div>
          <div class="content">
            <h1>FRNKROK</h1>
            <h4 class="thin">DJ/Producer</h4>
            <h4 class="thin">Mix/Mastering Engineer</h4>
            <div class="phone-link btn"><a href="http://superphone.io/f/qezOh6XY" target=_blank>Save Your Contact Info</a></div>

          </div>
        </div>
      </div>
    </div>
    <div class="client-logos">
      <div class="more-on-frank-btn" id="more-on-frank-btn">
        <div class="arrow-down"><span class="arrow">&darr;</span> &nbsp; CLIENTS</div>
      </div>
      <ul class="client-list">
        <li><img src="/pictures/clients/Chick-fila.png" alt="Chick Fil-a"></li>
        <li><img src="/pictures/clients/coca-cola_logo.png" alt="Coca Cola"></li>
        <li><img src="/pictures/clients/rix-lounge.png" alt="Rix Lounge"></li>
        <li><img src="/pictures/clients/redcoconut_logo.jpg" alt="Red Coconut"></li>
        <li><img src="/pictures/clients/3stardeluxe.jpg" alt="3 STAR DELUXE"></li>
        <li><img src="/pictures/clients/disney_springs.jpg" alt="Disney Coronado Springs"></li>
        <li><img src="/pictures/clients/backbooth.png" alt="Backbooth"></li>
        <li><img src="/pictures/clients/carrillo.jpg" alt="Carrillo"></li>
        <li><img src="/pictures/clients/citywalk.jpg" alt="City Walk"></li>
        <li><img src="/pictures/clients/EMlogo.png" alt="ElectroMagic"></li>
        <li><img src="/pictures/clients/beatgasm.jpg" alt="Beatgasm"></li>
        <li><img src="/pictures/clients/groove2.gif" alt="the Groove"></li>
        <!-- <li><img src="/pictures/clients/rix-lounge.jpeg" alt="Rix Lounge"></li> -->
        <li><img src="/pictures/clients/kings.JPG" alt="Kings"></li>
        <li><img src="/pictures/clients/kissy.jpg" alt="Kissy"></li>
        <li><img src="/pictures/clients/nbc_universal.jpg" alt="NBC Universal"></li>
        <li><img src="/pictures/clients/ournightlife-logo.png" alt="OUR NIGHT LIFE"></li>
        <li><img src="/pictures/clients/PowerLogo_vm_copy.png" alt="POWER 95.3"></li>
        <li><img src="/pictures/clients/promo_only_logo.png" alt="Promo Only"></li>
        <li><img src="/pictures/clients/universal-orlando.jpg" alt="Universal"></li>
        <li><img src="/pictures/clients/SplitsvilleLogo.jpg" alt="Splitsville"></li>
        <!-- <li><img src="/pictures/clients/sterling.jpg" alt="sterling"></li> -->
        <li><img src="/pictures/clients/thumb_Club23.jpg" alt="Club 23"></li>
        <li><img src="/pictures/clients/trend-micro-logo.png" alt="Trend Micro"></li>
        <li><img src="/pictures/clients/WDPR LOGO.png" alt="Walt Disney Parks"></li>
        <li><img src="/pictures/clients/Wendys-Logo.png" alt="Wendys"></li>
        <li><img src="/pictures/clients/Westin_logo_rgb.jpg" alt="Westin Hotels"></li>
        <li><img src="/pictures/clients/grainer.jpg" alt="Grainger"></li>
      </ul>
    </div>
  </section>
  <section class="events" id="social-block">
    <div class="events-container">
      <div class="events-center">
        <div class="wheel">
          <ul class="social-list">
            <li>
              <a href="https://www.soundcloud.com/FRNKROK" target="_blank">
                <div class="social-content">
                  <span class="symbol sc">circlesoundcloud</span>
                </div>
              </a>
            </li>
            <li>
              <a href="https://www.facebook.com/FRNKROK" target="_blank">
                <div class="social-content">
                  <span class="symbol fb">circlefacebook</span>
                </div>
              </a>
            </li>
            <li>
              <a href="https://instagram.com/frnkrok" target="_blank">
                <div class="social-content">
                  <span class="symbol ig">circleinstagram</span>
                </div>
              </a>
            </li>
            <li>
              <a href="https://twitter.com/frnkrok" target="_blank">
                <div class="social-content">
                  <span class="symbol tw">circletwitterbird</span>
                </div>
              </a>
            </li>
            <li>
              <a href="https://www.mixcloud.com/frnkrok/" target="_blank">
                <div class="social-content">
                  <img src="pictures/mixcloud.jpg" alt="Mixcloud">
                </div>
              </a>
            </li>
            <li>
              <a href="https://www.youtube.com/user/FRNKROKmusic" target="_blank">
                <div class="social-content">
                  <span class="symbol yt">circleyoutube</span>
                </div>
              </a>
            </li>
            <li>
              <a href="https://pro.beatport.com/artist/frnkrok/489836" target="_blank">
                <div class="social-content">
                  <img src="pictures/beatport.png" alt="Beatport">
                </div>
              </a>
            </li>
            <li>
              <a href="https://www.linkedin.com/in/frankgutierrezmusic" target="_blank">
                <div class="social-content">
                  <span class="symbol li">circlelinkedin</span>
                </div>
              </a>
            </li>
          </ul>

        </div>

      </div>
    </div>
  </section>
  <section class="events" id="mastering-block">
    <div class="events-container">
      <div class="events-center text-left">
          <div class="row-fake headline-row">
            <h2 class="oswald headline">FRNKROK MASTERING
              <div class="exit exit-to-home">&times;</div>
            </h2>
          </div>
          <div class="row-fake">
            <div class="container">
              <!-- <h1 class="color-alt">“Why We Are Better”
                <div class="big-btn-right">
                  <a class="big-btn" href="/mastering/start/">Start Project</a>
                </div>
              </h1> -->
              <h1 class="headline start mastering-headline">WHY WE ARE <span class="color-alt">BETTER</span></h1>

              <p class="lead">
                These days, there are many “One click” services that auto master your project for a fraction of the time and cost.
                However the results will only ever be as good as the mix down.
                Which is why working with a mastering engineer can also be a learning tool for the eager beginner &amp; the seasoned veteran.
                 A good mastering engineer will identify errors in your mix down, and give you the opportunity to correct before handing you back the finished product.
                  This ensures your master will be at its full potential when we return it to you.
              </p>
              <div class="big-btn-right">
                <a class="big-btn" href="/mastering/start/">Start Project</a>
              </div>
              <br>
              <div class="what-we-do-mastering">
                <h1 class="color-alt">“What We Do”</h1>
                <ul >
                  <li>Dj Mix/Set Mastering ($50)</li>
                  <li>Track Mastering ($50)</li>
                  <li>Stem Mastering ($5 per Stem)</li>
                  <li>Album Mastering (Quotes Available Upon request)</li>
                  <li>Full Track Mixdowns (Quotes Available Upon request)</li>
                </ul>
              </div>
              <p class="tagline-satisfaction">
                <a class="big-btn" href="/mastering/start/">Start Project</a>

                Turnarounds are 3-5 business days and we offer the opportunity to go back and retouch your project if you are not satisfied.
              </p>
              <div class="clear-float"></div>
              <h1 class="color-alt">“Formats Accepted &amp; Offered”</h1>
              <ul class="what-we-do-mastering">
                <li>44.1 .Wav Files @ 24bit (accepted &amp; offered)</li>
                <li>320kb MP3 (offered)</li>
              </ul>
              <div class="clear-float"></div>

            </div>
          </div>
      </div>
    </div>
  </section>

  <section class="events" id="tracks-block">
    <div class="events-container">
      <div class="events-center">
        <div class="album-container">
          <div class="row-fake">
            <h2 class="oswald headline">TRACKS
              <div class="exit exit-to-home">&times;</div>
            </h2>
          </div>
          <div class="row">
            <div class="container">

              <?php
                $tracks = file("content/tracks.txt");
                $activated = false;
                foreach ($tracks as $line_num => $line) {

                  if (substr($line,0,4) == trim("#end")) {
                    break;
                  }
                  if (substr($line,0,7) == trim("#tracks")) {
                    $activated = true;
                    continue;
                  }
                  if ( $activated ) {
                    $event = explode(";", $line);
                    if ( !$event[0] || !$event[1] ) {
                      continue;
                    }
                    ?>
                    <a href="<?php echo $event[1]; ?>" target="_blank">
                      <div class="col-3">
                        <div class="album-cvr" style="background-image: url(<?php echo $event[0]; ?>);"></div>
                      </div>
                    </a>
                    <?php
                  }
                }
              ?>


            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="events" id="edits-block">
    <div class="events-container">
      <div class="events-center">
        <div class="events-left">
          <ul class="events-list">
            <?php
              $events = file("content/edits.txt");
              $activatedLeft = $activatedRight = false;

              foreach ($events as $line_num => $line) {

                if (substr($line,0,4) == trim("#end")) {
                  break;
                }

                if (substr($line,0,12) == trim("#edits-right")) {
                  $activatedRight = true;
                  $activatedLeft = false;
                  ?>
                </ul>
              </div>
              <div class="events-right">
                <ul class="events-list">
                  <?php
                  continue;
                }

                if (substr($line,0,11) == trim("#edits-left")) {
                  $activatedLeft = true;
                  continue;
                }
                if ( $activatedLeft ) {
                  $event = explode(";", $line);
                  if ( !$event[0] || !$event[1] ) {
                    continue;
                  }
                  ?>
                  <a href="<?php echo $event[3]; ?>" target="_blank">
                    <li>
                      <strong class="date">
                        <?php echo $event[0]; ?>
                      </strong>
                      <span class="venue">
                        <?php echo $event[1]; ?>
                      </span>
                      <span class="loc">
                        <?php echo $event[2]; ?>
                      </span>
                    </li>
                  </a>
                  <?php
                }
                if ( $activatedRight ) {
                  $event = explode(";", $line);
                  if ( !$event[0] || !$event[1]) {
                    continue;
                  }
                  ?>
                  <a href="<?php echo $event[3]; ?>" target="_blank">
                    <li>
                      <strong class="date">
                        <?php echo $event[0]; ?>
                      </strong>
                      <span class="venue">
                        <?php echo $event[1]; ?>
                      </span>
                      <span class="loc">
                        <?php echo $event[2]; ?>
                      </span>
                    </li>
                  </a>
                  <?php
                }


              }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <section class="events" id="signup-block">
    <div class="events-container">
      <div class="events-center">
        <div class="email-container">
          <form id="subscribe-form">
            <div class="input-wrap">
              <input type="email" class="email-box" placeholder="Email address" id="email_input">
              <button type="submit" class="email-submit">></button>
            </div>
          </form>
        </div>
        <div class="strong">Enter your email and keep up with the latest from <u>FrnkRok</u></div>
        <div class="strong unsub" id="message_sub">Enter an email</div>
      </div>
    </div>
  </section>
  <script src="dist/main.js"></script>
</body>
</html>
